import * as Filter from './Filter';
import {combineReducers} from 'redux';

export const actionTypes = {
    Filter: Filter.actionTypes
};

export const actions = {
    Filter: Filter.actions
};

export const reducers = combineReducers({
    Filter: Filter.reducers
});

export const selectors = {
    Filter: Filter.selectors
};